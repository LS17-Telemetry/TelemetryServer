const updateIntervalMS = 20000
const maxNumDataPoints = 200;

var priceChart
$( document ).ready(function() {

    var ctx = document.getElementById("bigPriceChart").getContext('2d');
    priceChart = new Chart(ctx, {
                type: 'line',
                options: {
                  scales: {
                    xAxes: [{
                      display: false,
                      type: 'time',
                      time: {
                        displayFormats: {
                          second: 'MMM YYYY'
                        }
                      }
                    }],
            
                    yAxes: [{
                      display: true,
                    }]
                  },
                  legend: {
                    display: true,
                    boxWidth: 10,
                  }
                }
              });


      updateData();
});
bigPriceChart
function updateData() {
    $.getJSON('/json', function(data) {

        var dataByType = []
        var SellingPoints = data.Prices.SellingPoints
        
        for (i = 0; i < SellingPoints.length; i++) { 

            if(SellingPoints[i].FillType==null)
                continue;

            for (let tIdx = 0; tIdx < SellingPoints[i].FillType.length; tIdx++) {
                var spType = SellingPoints[i].FillType[tIdx];
                if(dataByType[spType.TypeID] == null) {
                    dataByType[spType.TypeID] = [spType.EffectivePrice, SellingPoints[i].Name, spType.Type, spType.TypeID];
                }
                else {
                    if(dataByType[spType.TypeID][0] < spType.EffectivePrice) {
                        dataByType[spType.TypeID] = [spType.EffectivePrice, SellingPoints[i].Name, spType.Type, spType.TypeID];
                    }
                }
            }
        }

        updateChartData(dataByType)
        setTimeout(updateData, updateIntervalMS);
    });
}


function updateChartData(priceData) {

    priceData.forEach(function (pData, index) {

        var ID = pData[3]
        var label = pData[2]
        var data =  pData[0]*1000
        var series = null

        //Check  if Series exist
        for (let idx = 0; idx < priceChart.data.datasets.length; idx++) {
            const e = priceChart.data.datasets[idx];
            if(e.label == label){
                series = priceChart.data.datasets[idx]
                break;
            }
        }

        if(series == null) {
            addSeriesToChart(label, {y: data, x: new Date()}, ID)
        }
        else {
            updateChartSeries(series, {y: data, x: new Date()})

            priceChart.update(.1);
        }    
    });

}


function updateChartSeries(series, data) {
    
    series.data.push(data);

    while (series.data.length > maxNumDataPoints) {
        series.data.shift();
    }

}


function addSeriesToChart(label, data, typeID) {
    
    var color=chartColorArray[typeID]

    if(color == null)
        color = DefaultColor

    priceChart.data.datasets.push({
        label: label,
        fill: false,
        backgroundColor: color,
        borderColor: color,
        pointRadius:1,
        data: [data]
    });

    priceChart.update(.1);

}
