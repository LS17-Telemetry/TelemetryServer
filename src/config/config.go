package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Configuration struct {
	Logger struct {
		Level    string `json:"Level"`
		Format   string `json:"Format"`
		Output   string `json:"Output"`
		Filename string `json:"Filename"`
		Rotation struct {
			MaxFileSizeMB  int  `json:"MaxFileSizeMB"`
			MaxNoOfBackups int  `json:"MaxNoOfBackups"`
			MaxAgeDays     int  `json:"MaxAgeDays"`
			Compress       bool `json:"Compress"`
		} `json:"Rotation"`
	} `json:"Logger"`
	Server struct {
		Host string `json:"Host"`
		Port int    `json:"Port"`
	} `json:"Server"`
	Game struct {
		Input string `json:"Input"`
	} `json:"Game"`
}

var Config Configuration

func init() {

	file, err := os.Open("config.json")
	if err != nil {
		log.Fatal("Cannot load Configfile:", err)
	}

	defer file.Close()
	decoder := json.NewDecoder(file)
	Config = Configuration{}
	err = decoder.Decode(&Config)
	if err != nil {
		log.Fatal("Cannot load Config:", err)
	}

	fmt.Println("Loaded config:")

	b, err := json.MarshalIndent(Config, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	os.Stdout.Write(b)
}
