package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	parser "xmlParser"
)

func ShowAnimals(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving ShowAnimals")

	t, err := template.ParseFiles("tpl/animals.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, map[string]string{"Msg": ""})
}

func ShowPricesPerType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving ShowPrices")

	t, err := template.ParseFiles("tpl/pricesPerType.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, map[string]string{"Msg": ""})
}

func ShowPrices(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving ShowPrices")

	t, err := template.ParseFiles("tpl/prices.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, map[string]string{"Msg": ""})
}

func ShowVehicle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving ShowVehicle")

	t, err := template.ParseFiles("tpl/vehicle.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, map[string]string{"Msg": ""})
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving index")

	t, err := template.ParseFiles("tpl/dashboard.html", "tpl/menu.html", "tpl/header.html", "tpl/footer.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, map[string]string{"Msg": ""})

}

func ShowRawJson(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving ShowRawJson")

	b, _ := json.MarshalIndent(parser.TeleData, "", "  ")

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
	w.Write(b)

}
