package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"", "GET", "/vehicle", ShowVehicle},
	Route{"", "GET", "/prices", ShowPrices},
	Route{"", "GET", "/pricesType", ShowPricesPerType},
	Route{"", "GET", "/animals", ShowAnimals},
	Route{"", "GET", "/json", ShowRawJson},
	Route{"", "GET", "/", Index},
	//	Route{"", "GET", "/addGarden", GardenCreate},
	//Route{"", "POST", "/saveGarden", GardenSave},
}
